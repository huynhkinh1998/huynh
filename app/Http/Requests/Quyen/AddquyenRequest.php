<?php

namespace App\Http\Requests\Quyen;

use Illuminate\Foundation\Http\FormRequest;

class AddquyenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ten_quyen'           =>  'required|min:3|max:30',
            'list_id_quyen'        =>  'required|min:0|max:30',
        ];
    }
    public function messages()
    {
        return [
            'ten_quyen.required'  =>  'Yêu cầu phải nhập tên quyền',
            'ten_khu.min'       =>  'Tên quyền phải từ 5 ký tự',
            'ten_khu.max'       =>  'Tên quyền tối đa được 30 ký tự',
            'list_id_quyen.*'      =>  'Yêu cầu phải nhập list id quyền !',
        ];
    }
}
