<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory as Faker;

class TestController extends Controller
{
    public function index()
    {
        return view('admin.page.demo');
    }
    public function testthongke()
    {
        return view('admin.page.thong_ke.index');
    }

    public function viewTaiKhoan()
    {
        return view('admin.page.tai_khoan.index');
    }

    public function viewLogin()
    {
        return view('admin.page.login');
    }
}
