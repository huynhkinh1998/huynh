<?php

namespace App\Http\Controllers;

use App\Http\Requests\Quyen\AddquyenRequest;
use App\Http\Requests\Quyen\UpdatequyenRequest;
use App\Models\Quyen1;
use Illuminate\Http\Request;

class Quyen1Controller extends Controller
{
    public function index()
    {
        return view('admin.page.quyen.index');
    }
    public function getData()
    {
        $list = Quyen1::get(); //KhuVuc::all();

        return response()->json([
            'list'  => $list
        ]);
    }
    public function store(AddquyenRequest $request)
    {
        $data = $request->all();

        Quyen1::create($data);

        return response()->json([
            'status'    => true,
            'message'   => 'Đã tạo mới quyền thành công!',
        ]);
    }
    public function destroy(Request $request)
    {
        $quyen = Quyen1::where('id', $request->id)->first();
        $quyen->delete();
        return response()->json([
            'status'    => true,
            'message'   => 'Đã xóa thành công!',
        ]);
    }
    public function update(UpdatequyenRequest $request)
    {
        $data    = $request->all();
        $quyen = Quyen1::find($request->id);
        $quyen->update($data);

        return response()->json([
            'status'    => true,
            'message'   => 'Đã cập nhật thành công!',
        ]);
    }

}
