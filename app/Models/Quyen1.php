<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quyen1 extends Model
{
    use HasFactory;
    protected $table = 'quyen1s';

    protected $fillable = [
        'ten_quyen',
        'list_id_quyen',
    ];
}
