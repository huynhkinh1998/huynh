@extends('admin.share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-4">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header">
                    <b>Thêm Quyền</b>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Tên Quyền</label>
                        <input v-model="add.ten_quyen" type="text" class="form-control" placeholder="Nhập vào tên quyền *">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">List Quyền</label>
                        <input v-model="add.list_id_quyen" type="text" class="form-control" placeholder="Nhập vào list id quyền *">
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button class="btn btn-primary" type="button" v-on:click="CreateTaiKhoan()">Tạo Mới</button>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header">
                    <b>Danh Sách Các Quyền</b>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle">#</th>
                                    <th class="text-center align-middle">Tên Quyền</th>
                                    <th class="text-center align-middle">List Quyền</th>
                                    <th class="text-center align-middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                             <tr v-for="(value , key) in list">
                                <th class="text-center align-middle">@{{key + 1}}</th>
                                <td class="text-center align-middle">@{{ value.ten_quyen}}</td>
                                <td class="text-center align-middle">@{{ value.list_id_quyen}}</td>
                                <td class="text-center align-middle text-nwrap">
                                    <button class="btn btn-info" v-on:click="edit = Object.assign({}, value)" data-bs-toggle="modal" data-bs-target="#updateModal">Cập Nhật</button>
                                    <button class="btn btn-danger" v-on:click="del = value" data-bs-toggle="modal"data-bs-target="#deleteModal">Xoá</button>
                                </td>
                             </tr>
                            </tbody>
                        </table>
                        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Xoá Quyền</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="alert alert-primary" role="alert">
                                            <p>Bạn có chắc chắn muốn xoá quyền <b class="text-danger">@{{del.ten_quyen}}</b> không?</p>
                                            <p><b>Lưu ý:</b> Thao tác này sẽ không thể hoàn tác</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Đóng</button>
                                        <button type="button" class="btn btn-danger" v-on:click="DelAdmin()">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Cập Nhật Quyền</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mb-3">
                                        <label class="form-label">Tên Quyền</label>
                                        <input v-model="edit.ten_quyen" type="text" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">List Quyền</label>
                                        <input v-model="edit.list_id_quyen" type="text" class="form-control">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button v-on:click="UpdateAdmin()" type="button" class="btn btn-warning">Xác Nhận</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    new Vue({
        el   : "#app",
        data : {
            add                 : {},
            list                : [],
            del                 : {},
            edit                : {},

        },
        created() {
            this.loadData();
        },
        methods : {

            CreateTaiKhoan(){
                axios
                    .post('/admin/quyen/create', this.add)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success(res.data.message);
                            this.loadData();
                            this.add = {};
                        }
                    })
                    .catch((res) => {
                        $.each(res.response.data.errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },
            loadData() {
                axios
                    .get('/admin/quyen/data')
                    .then((res) => {
                        this.list  =  res.data.list;
                    });
            },
            DelAdmin() {
                axios
                    .post('/admin/quyen/delete', this.del)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadData();
                            $('#deleteModal').modal('hide');
                        } else {
                            toastr.error(res.data.message);
                        }
                    })
                    .catch((res) => {
                        $.each(res.response.data.errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },
            UpdateAdmin() {
                axios
                    .post('/admin/quyen/update',this.edit)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success(res.data.message);
                            this.loadData();
                            $('#updateModal').modal('hide');
                        } else {
                            toastr.error(res.data.message);
                        }
                    })
                    .catch((res) => {
                        $.each(res.response.data.errors, function(k, v) {
                            toastr.error(v[0]);
                        })
                    });
            },
        }
    });
</script>
@endsection
